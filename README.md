# pve-vm-templates

Ansible playbook for building proxmox vm templates.

## Assumptions
- [x] Proxmox host has been configured
- [x] Proxmox storage has been configured

## System Prep
```bash
#!/bin/bash
# Clone repository recursively to include the make submodule
git clone --recursive git@gitlab.com:h2264/automation/ansible/jc-pve-templates.git

# Ensure that ssh access is preconfigured to proxmox host
vi ~/.ssh/config
```

## Execution
```bash
#!/bin/bash
make ansible-exec
```
This executes the ansible playbook `./src/build_templates.yaml`. Logical flow are as follows.
- Parse cloud image repositories and download build hash
- Compare downloaded build hash against reference hash.
- If hash differs, generate build script. Otherwise, skip.
- If build script exist, download image then build, otherwise skip.
- Clean downloadded hash file, template image and build script.

## Variables
### Shell
Override through `make $VARIABLENAME=$VALUE $MAKETARGET`. E.g. `make _ANSIBLE_SRC=./src ansible-lint`
| Name | Affected make targets | Comment |
| --- | --- | --- |
| _ANSIBLE_VENV | all | Virtualenv directory. Path where ansible is stored and referenced. |
| _ANSIBLE_SRC | all | Ansible source code root directory |
| _ANSIBLE_REQUIREMENTS | `ansible-do-systemprep` | Pip requirements file. |
| _ANSIBLE_LINT_YAML_RULES | `ansible-do-lint` | Pyyaml rules. Used for linting yaml files. |
| _ANSIBLE_EXEC_PLAYBOOK | `ansible-do-exec` | Relative path of the playbook to be executed. |
| _ANSIBLE_EXEC_INVENTORY | `ansible-do-exec` | Relative path of the playbook to be executed. Can be also set as `$IPV4,` |
| _ANSIBLE_EXEC_ENVVARS | `ansible-do-exec` | Placeholder for ansible env vars. |
| _ANSIBLE_EXEC_TAGS | `ansible-do-exec` | Set ansible tags. Only tasks or playbooks with these tags are executed. |
|||

### Ansible env vars
| Name | Comment |
| --- | --- |
| `download_path` | Download path in proxmox server. Default is `/tmp` |
| `vm_templates` | List of cloud image templates |
| `vm_templates.[].file` | Remote file name |
| `vm_templates.[].id` | Proxmox vm template ID |
| `vm_templates.[].memory` | Proxmox vm template memory |
| `vm_templates.[].name` | Proxmox vm template name |
| `vm_templates.[].storage` | Proxmox vm template storage ID |
| `vm_templates.[].template` | Build script template |
| `vm_templates.[].url` | Remote URL |
| `vm_templates.[].hash` | Remote hash file name |
|||

## CICD integration
### System prep
### Repository Layout
### Payloads
### Usage
